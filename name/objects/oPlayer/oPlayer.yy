{
    "id": "74803298-1840-41cc-8366-541c84880338",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "1e2a1ad3-ad86-4dd2-85ee-87471aa874f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "74803298-1840-41cc-8366-541c84880338"
        },
        {
            "id": "190efce4-d3ac-434f-a576-bb46292d5618",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "74803298-1840-41cc-8366-541c84880338"
        }
    ],
    "maskSpriteId": "d5d9de98-71f3-4ce8-95f5-b15aaee435bf",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d5d9de98-71f3-4ce8-95f5-b15aaee435bf",
    "visible": true
}