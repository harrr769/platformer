{
    "id": "d5d9de98-71f3-4ce8-95f5-b15aaee435bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 18,
    "bbox_right": 29,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80cf6a49-48bb-4caf-b0d8-9f0bd8d4ca3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5d9de98-71f3-4ce8-95f5-b15aaee435bf",
            "compositeImage": {
                "id": "c7b30605-2abc-4d9f-9055-9220620c7e63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80cf6a49-48bb-4caf-b0d8-9f0bd8d4ca3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b420260-54e7-482c-94b0-a7fb63242978",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80cf6a49-48bb-4caf-b0d8-9f0bd8d4ca3e",
                    "LayerId": "2ecde999-0754-40c2-9d87-0e6d4f55a3c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "2ecde999-0754-40c2-9d87-0e6d4f55a3c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5d9de98-71f3-4ce8-95f5-b15aaee435bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}