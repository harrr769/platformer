{
    "id": "f44feb43-c36a-44f0-8042-23042ae24d50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e1bae73-2e42-4b81-b36c-b1394f1fcd77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f44feb43-c36a-44f0-8042-23042ae24d50",
            "compositeImage": {
                "id": "25cd12ce-d626-436d-b803-be4fe95149ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e1bae73-2e42-4b81-b36c-b1394f1fcd77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3a94183-231e-47ef-86ff-9ade949e4157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e1bae73-2e42-4b81-b36c-b1394f1fcd77",
                    "LayerId": "65cdf70f-2d96-475d-b5d6-db8157f13046"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "65cdf70f-2d96-475d-b5d6-db8157f13046",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f44feb43-c36a-44f0-8042-23042ae24d50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}