{
    "id": "5c30dc12-5142-4bed-a415-4cd21a606535",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9556a5a-3e5e-4ad8-a2ac-8db51b280e75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c30dc12-5142-4bed-a415-4cd21a606535",
            "compositeImage": {
                "id": "08d4ccf2-5008-419e-8f0b-975a8481a0ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9556a5a-3e5e-4ad8-a2ac-8db51b280e75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d986f70-2ee5-4e36-b8e5-5c9622dc0945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9556a5a-3e5e-4ad8-a2ac-8db51b280e75",
                    "LayerId": "b8d1de98-f602-4e51-a6ae-b0a906a199cc"
                }
            ]
        },
        {
            "id": "0865ce2c-89f1-4b6d-a3c6-f3fafcd7f0ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c30dc12-5142-4bed-a415-4cd21a606535",
            "compositeImage": {
                "id": "6a8ba22a-8270-4488-a0b9-97ae2e1ecc2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0865ce2c-89f1-4b6d-a3c6-f3fafcd7f0ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e359dcb-5a61-40b8-a8fd-ab3eebe8cf3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0865ce2c-89f1-4b6d-a3c6-f3fafcd7f0ce",
                    "LayerId": "b8d1de98-f602-4e51-a6ae-b0a906a199cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "b8d1de98-f602-4e51-a6ae-b0a906a199cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c30dc12-5142-4bed-a415-4cd21a606535",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}