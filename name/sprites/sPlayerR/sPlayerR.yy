{
    "id": "33a85189-0b8f-4628-a5e0-c5bcda4a86c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0d89cbb-db75-46a3-b9ee-23001c8ab232",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33a85189-0b8f-4628-a5e0-c5bcda4a86c3",
            "compositeImage": {
                "id": "8394076a-7c1d-40aa-9fd8-285790077235",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0d89cbb-db75-46a3-b9ee-23001c8ab232",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4058a8c-ea14-4d0e-84fc-b83a48fd0ad6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0d89cbb-db75-46a3-b9ee-23001c8ab232",
                    "LayerId": "d49530bb-0906-4b37-8d9d-be1182288dc9"
                }
            ]
        },
        {
            "id": "1dfada2a-9e9a-4d32-97e3-4527c0afba47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33a85189-0b8f-4628-a5e0-c5bcda4a86c3",
            "compositeImage": {
                "id": "0bd658ed-5e5b-4978-96bd-c9ba051b33a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dfada2a-9e9a-4d32-97e3-4527c0afba47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87cadf86-3bae-41c2-bd88-30a01eb62f44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dfada2a-9e9a-4d32-97e3-4527c0afba47",
                    "LayerId": "d49530bb-0906-4b37-8d9d-be1182288dc9"
                }
            ]
        },
        {
            "id": "c2cb9c11-678f-43d9-8483-1d98511d80a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33a85189-0b8f-4628-a5e0-c5bcda4a86c3",
            "compositeImage": {
                "id": "d4e37e4c-4c6b-4625-a327-42a76c4e4deb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2cb9c11-678f-43d9-8483-1d98511d80a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6dd6ab9-b6e9-4344-9b19-b691ecd71036",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2cb9c11-678f-43d9-8483-1d98511d80a4",
                    "LayerId": "d49530bb-0906-4b37-8d9d-be1182288dc9"
                }
            ]
        },
        {
            "id": "946ab827-ddfb-41e3-9d0b-5bccaef6c29b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33a85189-0b8f-4628-a5e0-c5bcda4a86c3",
            "compositeImage": {
                "id": "80d3be5d-8157-414a-95db-af31b4f42459",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "946ab827-ddfb-41e3-9d0b-5bccaef6c29b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cbf613e-6b02-47fb-a994-d35fd3a69a1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "946ab827-ddfb-41e3-9d0b-5bccaef6c29b",
                    "LayerId": "d49530bb-0906-4b37-8d9d-be1182288dc9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "d49530bb-0906-4b37-8d9d-be1182288dc9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33a85189-0b8f-4628-a5e0-c5bcda4a86c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}